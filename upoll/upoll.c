#include <asm/uaccess.h>
#include <linux/debugfs.h>
#include <linux/delay.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/module.h>
#include <linux/poll.h>
#include <linux/wait.h>
#include <linux/jiffies.h>
#include <linux/irq.h>
#include <linux/device.h>
#include <linux/cdev.h>
#include <linux/interrupt.h>
#include <uapi/linux/stat.h>

#include <linux/platform_device.h>
#include <linux/interrupt.h>

MODULE_LICENSE("GPL");

#define PIO_IRQ_NUM 38

#define DRIVER_NAME "pio_intr"

static struct semaphore g_probe_sem;
static dev_t g_dev_no = 0;
static struct cdev * upoll_cdev = NULL;
static struct class * upoll_class = NULL;
static int g_upoll_baseaddr = 0;
static int g_upoll_size = 0;
static int g_upoll_irq = 0;

static void __iomem *g_upoll_va_base;

static wait_queue_head_t waitqueue;
static atomic_t nevents;

static ssize_t upoll_read(struct file *filp, char __user *buf, size_t len, loff_t *off)
{
/*
	ssize_t ret;

	if (copy_to_user(buf, readbuf, readbuflen)) {
		ret = -EFAULT;
	} else {
		ret = readbuflen;
	}

	readbuflen = 0;

	return ret;
*/
	return 0;
}

unsigned int upoll_poll(struct file *filp, struct poll_table_struct *wait)
{

	poll_wait(filp, &waitqueue, wait);
	if (atomic_read(&nevents) > 0)
	{
		atomic_sub(1, &nevents);
		return POLLIN;
	}
	return 0;
}

irqreturn_t upoll_isr(int this_irq, void *dev_id)
{
	atomic_inc(&nevents);
	pr_info("Interrupt received\n");
	wake_up_interruptible(&waitqueue);
	iowrite32(0xf, g_upoll_va_base + 0xc);

	return IRQ_HANDLED;
}

static const struct file_operations fops = {
	.owner = THIS_MODULE,
	.read = upoll_read,
	.poll = upoll_poll
};



static int upoll_platform_probe(struct platform_device *pdev)
{
	int ret;
	struct resource *rsc;
	struct resource *upoll_driver_mem_region;
	int irq;
	dev_t dev_no;

	pr_info("platform_probe enter\n");

	ret = -EBUSY;


	if (down_interruptible(&g_probe_sem))
		return -ERESTARTSYS;

	ret = -EINVAL;
	
	rsc = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (rsc == NULL) {
		pr_err ("IORESOURCE_MEM, 0 does not exist\n");
		return ret;
	}

	g_upoll_baseaddr = rsc->start;
	g_upoll_size = resource_size(rsc);

	upoll_driver_mem_region = request_mem_region (g_upoll_baseaddr, g_upoll_size, "UPOLL_HW_REGION");
	if (upoll_driver_mem_region == NULL ) {
		pr_err("request_mem_region failed\n");
		return -EINVAL;
	}
	g_upoll_va_base = ioremap(g_upoll_baseaddr, g_upoll_size);

	if (g_upoll_va_base == NULL) {
		pr_err("ioremap failed\n");
		return -EINVAL;
	}

	irq = platform_get_irq(pdev, 0);
	if (irq < 0) {
		pr_err("IRQ not avaiable\n");
		return -EINVAL;
	}

	g_upoll_irq = irq;
	init_waitqueue_head(&waitqueue);

	if (ret = request_irq(g_upoll_irq, upoll_isr, 0, 0, 0)) {
		pr_err("Failure requesting irq %i\n", irq);
	}
	
	ret = alloc_chrdev_region(&dev_no, 0, 1, DRIVER_NAME);
	if (ret <0) {
		pr_err("alloc_chrdev_regiosn() failed\n");
		return ret;
	}
	g_dev_no = dev_no;

	upoll_class = class_create(THIS_MODULE, DRIVER_NAME);

	upoll_cdev = cdev_alloc();
	upoll_cdev->ops = &fops;
	upoll_cdev->owner = THIS_MODULE;

	ret = cdev_add(upoll_cdev, g_dev_no, 1);
	if (ret <0) {
		pr_err("failed to call cdev_add()\n");
	}

	device_create(upoll_class, NULL, g_dev_no, NULL, DRIVER_NAME);

	up(&g_probe_sem);
	return 0;
}

static int upoll_platform_remove(struct platform_device *pdev)
{
	int ret;

	pr_info("platform_remove enter\n");
	free_irq(g_upoll_irq, 0);
	device_destroy(upoll_class, g_dev_no);

}

static struct of_device_id upoll_driver_dt_ids[] = {
	{ .compatible = "uqk,upoll-driver1.0" },
	{},
};

MODULE_DEVICE_TABLE(of, upoll_driver_dt_ids);

static struct platform_driver upoll_platform_driver = {
	.probe = upoll_platform_probe,
	.remove = upoll_platform_remove,
	.driver = {
		.name = "upoll driver",
		.owner = THIS_MODULE,
		.of_match_table = upoll_driver_dt_ids,
	},
};

static int __init upoll_init(void)
{

	int ret;

	pr_info("%s enter\n", __FUNCTION__);
	sema_init(&g_probe_sem, 1);
	
	ret = platform_driver_register(&upoll_platform_driver);
	if (ret != 0) {
		pr_err("failed to call platform_driver_register %d\n", ret);
		return ret;
	}

	pr_info("%s exit\n", __FUNCTION__);
	return 0;
}

static void __exit upoll_exit(void)
{
	pr_info("upoll_exit enter\n");
	platform_driver_unregister(&upoll_platform_driver);
	pr_info("upoll_exit exit\n");
}


module_init(upoll_init);
module_exit(upoll_exit);
