#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <sys/mman.h> 


#define H2F_BRIDGE_BASE	0xff200000
#define H2F_BRIDGE_SPAN	0x200000

int enable_pio_interrupt(void)
{
	volatile void *pshmem = (void *)NULL;
	int fd = 0;

	if ((fd = open("/dev/mem", (O_RDWR | O_SYNC))) == -1) {
		perror("open");
		return -1;
	}

	pshmem = mmap(NULL, H2F_BRIDGE_SPAN, (PROT_READ | PROT_WRITE), MAP_SHARED, fd, H2F_BRIDGE_BASE); 

	if (pshmem == MAP_FAILED) {
		perror("failed to mmap");
		close(fd);
		return -2;
	}

	*((unsigned long *)(pshmem + 0x28)) = 0xf;	/* enabling interupt */
	return 0;
}

int main(int argc, char const *argv[])
{
    char buf[1024], path[1024];
    int fd, i, n;
    short revents;
    struct pollfd pfd;

	enable_pio_interrupt();

    fd = open(argv[1], O_RDONLY | O_NONBLOCK);

    if (fd == -1) {
        perror("open");
        exit(EXIT_FAILURE);
    }

    pfd.fd = fd;
    pfd.events = POLLIN;

    while(1) {
        puts("loop");
        i = poll(&pfd, 1, -1);
        if (i == -1) {
            perror("poll");
            exit(EXIT_FAILURE);
        }
        revents = pfd.revents;
        if (revents & POLLIN) {
            n = read(pfd.fd, buf, sizeof(buf));
            printf("Interrupt Captured\n");
        }
    }
    return 0;
}
